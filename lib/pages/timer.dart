import 'dart:async';

import 'package:flutter/material.dart';
import 'package:google_mobile_ads/google_mobile_ads.dart';
import 'package:percent_indicator/circular_percent_indicator.dart';
import 'package:timer.dart/controllers/timer.dart';
import 'package:timer.dart/molecules/number_strip.dart';
import 'package:keep_screen_on/keep_screen_on.dart';
import 'package:soundpool/soundpool.dart';
import 'package:flutter/services.dart';

import '../ad_helper.dart';
import '../main.dart';
import '../utils.dart';

class TimerPage extends StatefulWidget {
  final int rounds;
  final Duration prepareDuration;
  final Duration roundDuration;
  final Duration restDuration;
  final int endRoundWarningSecond;

  const TimerPage(
      {super.key,
      required this.rounds,
      required this.prepareDuration,
      required this.roundDuration,
      required this.restDuration,
      required this.endRoundWarningSecond});

  @override
  State<TimerPage> createState() => _TimerPageState();
}

class _TimerPageState extends State<TimerPage> {
  Soundpool? _pool;
  final SoundpoolOptions _soundpoolOptions = SoundpoolOptions();
  late TimerController timer;
  late Future<int> _beepLow;
  late Future<int> _beepHigh;
  late Future<int> _buzzer;
  late Future<int> _tack;

  @override
  void initState() {
    super.initState();
    timer = TimerController(widget.rounds, widget.prepareDuration,
        widget.roundDuration, widget.restDuration);
    timer.addListener(_listenTimer);
    _pool = Soundpool.fromOptions(options: _soundpoolOptions);
    _beepLow = _loadBeepLow();
    _beepHigh = _loadBeepHigh();
    _buzzer = _loadBuzzer();
    _tack = _loadTack();
  }

  Future<int> _loadTack() async {
    var asset = await rootBundle.load("assets/sounds/tack.mp3");
    return await _pool!.load(asset);
  }

  _playTack() async {
    var sound = await _tack;
    Future.delayed(const Duration(milliseconds: 0), () => _pool!.play(sound));
    Future.delayed(const Duration(milliseconds: 200), () => _pool!.play(sound));
    Future.delayed(const Duration(milliseconds: 400), () => _pool!.play(sound));

  }

  Future<int> _loadBuzzer() async {
    var asset = await rootBundle.load("assets/sounds/buzzer.mp3");
    return await _pool!.load(asset);
  }

  _playBuzzer() async {
    var sound = await _buzzer;
    _pool!.play(sound);
  }

  Future<int> _loadBeepLow() async {
    var asset = await rootBundle.load("assets/sounds/beep-low.mp3");
    return await _pool!.load(asset);
  }

  _playBeepLow() async {
    var sound = await _beepLow;
    _pool!.play(sound);
  }

  Future<int> _loadBeepHigh() async {
    var asset = await rootBundle.load("assets/sounds/beep-high.mp3");
    return await _pool!.load(asset);
  }

  _playBeepHigh() async {
    var sound = await _beepHigh;
    _pool!.play(sound);
  }

  @override
  dispose() {
    KeepScreenOn.turnOff();
    _pool?.dispose();
    super.dispose();
  }

  _listenTimer() {
    if (!mounted) {
      return;
    }
    setState(() {
      handleRoundState();
    });
  }

  String _roundStateName() {
    switch (timer.currentState) {
      case RoundState.waiting:
        return "";
      case RoundState.prepare:
        return "Ready?";
      case RoundState.done:
        return "Done!";
      case RoundState.round:
        return "Go!";
      case RoundState.rest:
        return "Rest Time";
    }
  }

  void handleRoundState() {
    if (timer.currentState == RoundState.round) {
      if (timer.currentDuration.inSeconds == 0) {
          _playBuzzer();
      } else if (timer.currentDuration.inSeconds == widget.endRoundWarningSecond) {
        _playTack();
      }
    } else {
      switch (timer.currentDuration.inSeconds) {
        case 3:
          _playBeepLow();
          break;
        case 2:
          _playBeepLow();
          break;
        case 1:
          _playBeepLow();
          break;
        case 0:
          _playBeepHigh();
      }
    }
  }

  void startTimer(){
    KeepScreenOn.turnOn();
    timer.startTimer();
  }

  void pauseTimer(){
    KeepScreenOn.turnOff();
    timer.pauseTimer();
  }

  @override
  Widget build(BuildContext context) {
    ThemeData theme = Theme.of(context);
    final textTheme = theme.textTheme;
    final MyColors? myColors = theme.extension<MyColors>();
    bool timerRunning = timer.isTimerRunning();
    Color? background = theme.colorScheme.background;
    int currentTimerTotalTime = timer.currentDuration.inSeconds;
    if (timer.currentState == RoundState.prepare) {
      background = myColors?.prepareColor;
      currentTimerTotalTime = timer.prepareDuration.inSeconds;
    }
    if (timer.currentState == RoundState.round) {
      background = myColors?.roundColor;
      currentTimerTotalTime = timer.roundDuration.inSeconds;
    }
    if (timer.currentState == RoundState.rest) {
      background = myColors?.restColor;
      currentTimerTotalTime = timer.restDuration.inSeconds;
    }
    final timerPercent = Utils.getTimerPercent(
        currentTimerTotalTime, timer.currentDuration.inSeconds);
    return Scaffold(
      backgroundColor: background,
      appBar: AppBar(
        centerTitle: true,
        title: const Text('CronoTrainer'),
      ),
      body: Center(
        child: Column(
          children: [
            Text(
              "Round",
              style: textTheme.displayMedium,
            ),
            NumberStrip(timer.currentRound, timer.rounds),
            Text(
              _roundStateName(),
              style: textTheme.displaySmall,
            ),
            Padding(
              padding: const EdgeInsets.all(20.0),
              child: LayoutBuilder(
                builder: (BuildContext context, BoxConstraints constraints) {
                  return CircularPercentIndicator(
                    radius: (constraints.maxWidth > constraints.maxHeight
                            ? constraints.maxHeight
                            : constraints.maxWidth) /
                        2,
                    lineWidth: 25.0,
                    percent: timerPercent,
                    center: Text(
                      Utils.printDuration(timer.currentDuration),
                      style: textTheme.displayLarge,
                    ),
                    circularStrokeCap: CircularStrokeCap.butt,
                    progressColor: timer.currentState == RoundState.waiting || timer.currentState == RoundState.done
                        ? theme.colorScheme.primary
                        : theme.colorScheme.background,
                  );
                },
              ),
            ),
            const SizedBox(
              height: 50,
            ),
            ElevatedButton(
              onPressed: () =>
                  timerRunning ? pauseTimer() : startTimer(),
              child: timerRunning
                  ? const Icon(Icons.pause)
                  : const Icon(Icons.play_arrow),
            ),
          ],
        ),
      ),
    );
  }
}
