import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:google_mobile_ads/google_mobile_ads.dart';
import 'package:numberpicker/numberpicker.dart';
import '../ad_helper.dart';
import 'timer.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class HomePage extends StatefulWidget {
  const HomePage({super.key});

  @override
  State<HomePage> createState() => _HomePageState();
}

enum RoundState { prepare, round, rest }

class _HomePageState extends State<HomePage> {
  int rounds = 3;
  Duration prepareDuration = const Duration(minutes: 0, seconds: 5);
  Duration roundDuration = const Duration(minutes: 0, seconds: 45);
  Duration restDuration = const Duration(minutes: 0, seconds: 15);
  int endRoundWarningSecond = 10;
  BannerAd? _bannerAd;

  @override
  void initState() {
    super.initState();
    BannerAd(
      adUnitId: AdHelper.bannerAdUnitId,
      request: const AdRequest(),
      size: AdSize.banner,
      listener: BannerAdListener(
        onAdLoaded: (ad) {
          setState(() {
            _bannerAd = ad as BannerAd;
          });
        },
        onAdFailedToLoad: (ad, err) {
          print('Failed to load a banner ad: ${err.message}');
          ad.dispose();
        },
      ),
    ).load();
  }

  void _showDialog(Widget child) {
    showCupertinoModalPopup<void>(
      context: context,
      builder: (BuildContext context) => Container(
        height: 216,
        padding: const EdgeInsets.only(top: 6.0),
        // The bottom margin is provided to align the popup above the system
        // navigation bar.
        margin: EdgeInsets.only(
          bottom: MediaQuery.of(context).viewInsets.bottom,
        ),
        // Provide a background color for the popup.
        color: CupertinoColors.systemBackground.resolveFrom(context),
        // Use a SafeArea widget to avoid system overlaps.
        child: SafeArea(
          top: false,
          child: child,
        ),
      ),
    );
  }

  String _printDuration(Duration duration) {
    String twoDigits(int n) => n.toString().padLeft(2, "0");
    String twoDigitMinutes = twoDigits(duration.inMinutes.remainder(60).abs());
    String twoDigitSeconds = twoDigits(duration.inSeconds.remainder(60).abs());
    return "$twoDigitMinutes:$twoDigitSeconds";
  }

  String _sumTime() {
    Duration total = Duration(
        seconds: roundDuration.inSeconds * rounds +
            restDuration.inSeconds * (rounds - 1));
    return _printDuration(total);
  }

  @override
  dispose() {
    _bannerAd?.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    ThemeData theme = Theme.of(context);
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text(AppLocalizations.of(context)!.home_title),
      ),
      body: Center(
        child: Column(
          children: [
            Container(
              color: theme.colorScheme.primary,
              child: Row(
                children: [
                  Expanded(
                    child: Column(
                      children: [
                        Text(
                          _sumTime(),
                          style: const TextStyle(
                              fontSize: 22.0, color: Colors.white),
                        ),
                        const SizedBox(
                          height: 10,
                        ),
                      ],
                    ),
                  )
                ],
              ),
            ),
            const SizedBox(
              height: 20,
            ),
            Text(AppLocalizations.of(context)!.home_prep_time),
            CupertinoButton(
              onPressed: () => _showDialog(
                CupertinoTimerPicker(
                  mode: CupertinoTimerPickerMode.ms,
                  initialTimerDuration: prepareDuration,
                  onTimerDurationChanged: (Duration newDuration) {
                    setState(() => prepareDuration = newDuration);
                  },
                ),
              ),
              child: Text(
                _printDuration(prepareDuration),
                style: const TextStyle(
                  fontSize: 22.0,
                ),
              ),
            ),
            Text(AppLocalizations.of(context)!.round_time),
            CupertinoButton(
              onPressed: () => _showDialog(
                CupertinoTimerPicker(
                  mode: CupertinoTimerPickerMode.ms,
                  initialTimerDuration: roundDuration,
                  onTimerDurationChanged: (Duration newDuration) {
                    setState(() {
                      roundDuration = newDuration;
                      if (endRoundWarningSecond > roundDuration.inSeconds) {
                        endRoundWarningSecond = roundDuration.inSeconds;
                      }
                    });
                  },
                ),
              ),
              child: Text(
                _printDuration(roundDuration),
                style: const TextStyle(
                  fontSize: 22.0,
                ),
              ),
            ),
            Text(AppLocalizations.of(context)!.round_end_warning),
            const SizedBox(
              height: 5,
            ),
            NumberPicker(
              value: endRoundWarningSecond,
              minValue: 0,
              maxValue: roundDuration.inSeconds,
              step: 1,
              haptics: true,
              axis: Axis.horizontal,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(16),
                border:
                    Border.all(color: theme.colorScheme.primary, width: 1.5),
              ),
              onChanged: (value) =>
                  setState(() => endRoundWarningSecond = value),
            ),
            const SizedBox(
              height: 10,
            ),
            Text(AppLocalizations.of(context)!.rest_time),
            CupertinoButton(
              onPressed: () => _showDialog(
                CupertinoTimerPicker(
                  mode: CupertinoTimerPickerMode.ms,
                  initialTimerDuration: restDuration,
                  onTimerDurationChanged: (Duration newDuration) {
                    setState(() => restDuration = newDuration);
                  },
                ),
              ),
              child: Text(
                _printDuration(restDuration),
                style: const TextStyle(
                  fontSize: 22.0,
                ),
              ),
            ),
            Text(AppLocalizations.of(context)!.rounds),
            const SizedBox(
              height: 5,
            ),
            NumberPicker(
              value: rounds,
              minValue: 1,
              maxValue: 100,
              step: 1,
              haptics: true,
              axis: Axis.horizontal,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(16),
                border:
                    Border.all(color: theme.colorScheme.primary, width: 1.5),
              ),
              onChanged: (value) => setState(() => rounds = value),
            ),
            const SizedBox(
              height: 20,
            ),
            const SizedBox(
              height: 20,
            ),
            ElevatedButton.icon(
              icon: const Icon(Icons.play_arrow),
              label: Text(
                AppLocalizations.of(context)!.home_button,
                style: const TextStyle(
                  fontSize: 30,
                ),
              ),
              onPressed: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => TimerPage(
                        rounds: rounds,
                        prepareDuration: prepareDuration,
                        roundDuration: roundDuration,
                        restDuration: restDuration,
                        endRoundWarningSecond: endRoundWarningSecond),
                  ),
                );
              },
            ),
            const SizedBox(
              height: 20,
            ),
            if (_bannerAd != null)
              Align(
                alignment: Alignment.bottomCenter,
                child: SizedBox(
                  width: _bannerAd!.size.width.toDouble(),
                  height: _bannerAd!.size.height.toDouble(),
                  child: AdWidget(ad: _bannerAd!),
                ),
              ),
          ],
        ),
      ),
    );
  }
}
