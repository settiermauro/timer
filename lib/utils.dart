class Utils{
  static String printDuration(Duration? duration) {
    if(duration == null) return "00:00";
    String twoDigits(int n) => n.toString().padLeft(2, "0");
    String twoDigitMinutes = twoDigits(duration.inMinutes.remainder(60).abs());
    String twoDigitSeconds = twoDigits(duration.inSeconds.remainder(60).abs());
    return "$twoDigitMinutes:$twoDigitSeconds";
  }

  static double getTimerPercent(totalTime, remainingTime){
    return remainingTime / totalTime;
  }
}