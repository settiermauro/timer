import 'package:flutter/material.dart';

class NumberStrip extends StatelessWidget {
  final int currentNumber;
  final int maxNumber;

  const NumberStrip(this.currentNumber, this.maxNumber, {super.key});

  @override
  Widget build(BuildContext context) {
    ThemeData theme = Theme.of(context);
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: [
        Opacity(
          opacity: currentNumber > 1 ? 1 : 0,
          child: Padding(
            padding: const EdgeInsets.all(10),
            child: Text(
              (currentNumber - 1).toString(),
              style: theme.textTheme.displaySmall,
            ),
          ),
        ),
        DecoratedBox(
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(16),
            border: Border.all(color: theme.colorScheme.primary, width: 2),
          ),
          child: Padding(
            padding:
                const EdgeInsets.symmetric(vertical: 8.0, horizontal: 20.0),
            child: Text(currentNumber.toString(),
                style: theme.textTheme.displayMedium),
          ),
        ),
        Opacity(
          opacity: currentNumber < maxNumber? 1 : 0,
          child: Padding(
            padding: const EdgeInsets.all(10),
            child: Text((currentNumber + 1).toString(),
                style: theme.textTheme.displaySmall),
          ),
        ),
      ],
    );
  }
}
