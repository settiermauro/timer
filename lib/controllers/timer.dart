import 'dart:async';

import 'package:flutter/cupertino.dart';

enum RoundState { waiting, prepare, round, rest, done }

class TimerController extends ChangeNotifier {
  final int rounds;
  final Duration prepareDuration;
  final Duration roundDuration;
  final Duration restDuration;
  Timer? countdownTimer;
  int currentRound = 1;
  late Duration currentDuration;
  RoundState currentState = RoundState.waiting;

  TimerController(this.rounds, this.prepareDuration, this.roundDuration,
      this.restDuration) {
    currentDuration = Duration(seconds: prepareDuration.inSeconds);
  }

  @override
  dispose() {
    countdownTimer?.cancel();
    super.dispose();
  }

  void startTimer() {
    if (countdownTimer != null) countdownTimer!.cancel();
    if (currentState == RoundState.waiting || currentState == RoundState.done) {
      currentState = RoundState.prepare;
      currentDuration = Duration(seconds: prepareDuration.inSeconds);
      currentRound = 1;
    }
    countdownTimer =
        Timer.periodic(const Duration(seconds: 1), (_) => setCountDown());
    notifyListeners();
  }

  void pauseTimer() {
    countdownTimer?.cancel();
    notifyListeners();
  }

  void setCountDown() {
    const reduceSecondsBy = 1;
    final seconds = currentDuration.inSeconds - reduceSecondsBy;
    if (seconds < 0) {
      nextRoundStata();
    } else {
      currentDuration = Duration(seconds: seconds);
    }
    notifyListeners();
  }

  void nextRoundStata() {
    countdownTimer!.cancel();
    RoundState newState = RoundState.round;
    int newRound = currentRound;
    switch (currentState) {
      case RoundState.prepare:
        newState = RoundState.round;
        break;
      case RoundState.round:
        newState = RoundState.rest;
        break;
      case RoundState.rest:
        newState = RoundState.round;
        break;
    }
    if (newState == RoundState.round) {
      currentDuration = Duration(seconds: roundDuration.inSeconds);
      if (currentState != RoundState.prepare) newRound++;
    }
    if (newState == RoundState.rest) {
      currentDuration = Duration(seconds: restDuration.inSeconds);
    }
    if (newRound == rounds && newState == RoundState.rest) {
      currentState = RoundState.done;
    } else {
      currentState = newState;
      currentRound = newRound;
      startTimer();
    }
  }

  bool isTimerRunning() {
    return countdownTimer != null && countdownTimer!.isActive;
  }
}
