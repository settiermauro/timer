import 'dart:async';

import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:google_mobile_ads/google_mobile_ads.dart';
import 'pages/home.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

@immutable
class MyColors extends ThemeExtension<MyColors> {
  const MyColors({
    required this.prepareColor,
    required this.roundColor,
    required this.restColor,
  });

  final Color? prepareColor;
  final Color? roundColor;
  final Color? restColor;

  @override
  MyColors copyWith(
      {Color? prepareColor, Color? roundColor, Color? restColor}) {
    return MyColors(
      prepareColor: prepareColor ?? this.prepareColor,
      roundColor: roundColor ?? this.roundColor,
      restColor: restColor ?? this.restColor,
    );
  }

  @override
  MyColors lerp(MyColors? other, double t) {
    if (other is! MyColors) {
      return this;
    }
    return MyColors(
      prepareColor: Color.lerp(prepareColor, other.prepareColor, t),
      roundColor: Color.lerp(roundColor, other.roundColor, t),
      restColor: Color.lerp(roundColor, other.roundColor, t),
    );
  }
}

void main() {
  WidgetsFlutterBinding.ensureInitialized();
  unawaited(MobileAds.instance.initialize());
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    ColorScheme colorScheme = ColorScheme.fromSeed(
      seedColor: const Color(0xFFBC7FCD),
      brightness: Brightness.light,
    );
    return MaterialApp(
      title: 'CronoTrainer',
      theme: ThemeData(
        useMaterial3: true,
        // Define the default brightness and colors.
        colorScheme: colorScheme,
        appBarTheme: AppBarTheme(
          backgroundColor: colorScheme.primary,
          foregroundColor: Colors.white
        ),
        textTheme: GoogleFonts.robotoTextTheme(
          Theme.of(context).textTheme,
        ).copyWith(
          displayLarge: TextStyle(
              fontSize: 72.0,
              fontWeight: FontWeight.bold,
              color: colorScheme.secondary),
        ),
      ).copyWith(
        extensions: <ThemeExtension<dynamic>>[
          const MyColors(
              prepareColor: Color(0xFFFFCF96),
              roundColor: Color(0xFFCDFAD5),
              restColor: Color(0xFFFF8080)),
        ],
      ),
      localizationsDelegates: const [
        AppLocalizations.delegate,
        GlobalMaterialLocalizations.delegate,
        GlobalWidgetsLocalizations.delegate,
        GlobalCupertinoLocalizations.delegate,
      ],
      supportedLocales: const [
        Locale('en'),
        Locale('es'),
      ],
      home: const HomePage(),
    );
  }
}
