import 'package:flutter_test/flutter_test.dart';
import 'package:timer.dart/utils.dart';

void main() {
  test('begin 100%', () async {
    expect(Utils.getTimerPercent(15, 15), 1.0);
  });
  test('end 0%', () async {
    expect(Utils.getTimerPercent(15, 0), 0.0);
  });
  test('mid 50%', () async {
    expect(Utils.getTimerPercent(20, 10), 0.5);
  });
  test('9 remaining in 10 total is 90%', () async {
    expect(Utils.getTimerPercent(10, 9), 0.9);
  });
  test('1 remaining in 10 total is 10%', () async {
    expect(Utils.getTimerPercent(10, 1), 0.1);
  });
}
