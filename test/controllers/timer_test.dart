import 'package:fake_async/fake_async.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:timer.dart/controllers/timer.dart';

void main() {
  test('timer normal run', () async {
    fakeAsync((FakeAsync async) {
      const rounds = 10;
      const prepareDuration = Duration(seconds: 9);
      const roundDuration = Duration(seconds: 60);
      const restDuration = Duration(seconds: 15);
      TimerController timer =
          TimerController(rounds, prepareDuration, roundDuration, restDuration);

      expect(timer.currentState, RoundState.waiting);
      expect(timer.currentDuration.inSeconds, prepareDuration.inSeconds);

      timer.startTimer();
      expect(timer.currentState, RoundState.prepare);

      async.elapse(const Duration(seconds: 3));
      expect(timer.currentDuration.inSeconds, prepareDuration.inSeconds - 3);

      async.elapse(const Duration(seconds: 6));
      expect(timer.currentDuration.inSeconds, 0);
      expect(timer.currentState, RoundState.prepare);

      async.elapse(const Duration(seconds: 1));
      for (int i = 1; i <= rounds; i++) {
        expect(timer.currentRound, i);
        expect(timer.currentDuration.inSeconds, roundDuration.inSeconds);
        expect(timer.currentState, RoundState.round);

        async.elapse(Duration(seconds: roundDuration.inSeconds + 1));
        if (i == rounds) {
          expect(timer.currentState, RoundState.done);
        } else {
          expect(timer.currentDuration.inSeconds, restDuration.inSeconds);
          expect(timer.currentState, RoundState.rest);
          async.elapse(Duration(seconds: restDuration.inSeconds + 1));
        }
      }
    });
  });

  test('timer start after completed run', () async {
    fakeAsync((FakeAsync async) {
      const rounds = 2;
      const prepareDuration = Duration(seconds: 9);
      const roundDuration = Duration(seconds: 60);
      const restDuration = Duration(seconds: 15);
      TimerController timer =
          TimerController(rounds, prepareDuration, roundDuration, restDuration);

      timer.startTimer();

      async.elapse(Duration(seconds: prepareDuration.inSeconds + 1));
      expect(timer.currentRound, 1);
      expect(timer.currentDuration.inSeconds, roundDuration.inSeconds);
      expect(timer.currentState, RoundState.round);

      async.elapse(Duration(seconds: roundDuration.inSeconds + 1));

      async.elapse(Duration(seconds: restDuration.inSeconds + 1));
      expect(timer.currentRound, 2);
      async.elapse(Duration(seconds: roundDuration.inSeconds + 1));
      expect(timer.currentState, RoundState.done);

      timer.startTimer();
      expect(timer.currentRound, 1);
      expect(timer.currentState, RoundState.prepare);
      expect(timer.currentDuration.inSeconds, prepareDuration.inSeconds);
    });
  });

  test('timer pause and start', () async {
    fakeAsync((FakeAsync async) {
      const rounds = 2;
      const prepareDuration = Duration(seconds: 9);
      const roundDuration = Duration(seconds: 60);
      const restDuration = Duration(seconds: 15);
      TimerController timer =
      TimerController(rounds, prepareDuration, roundDuration, restDuration);

      timer.startTimer();

      async.elapse(Duration(seconds: prepareDuration.inSeconds - 4));
      expect(timer.currentRound, 1);
      expect(timer.currentDuration.inSeconds, 4);
      expect(timer.currentState, RoundState.prepare);

      timer.pauseTimer();

      async.elapse(Duration(seconds: prepareDuration.inSeconds - 4));
      expect(timer.currentRound, 1);
      expect(timer.currentDuration.inSeconds, 4);
      expect(timer.currentState, RoundState.prepare);

      timer.startTimer();

      async.elapse(const Duration(seconds: 5));
      expect(timer.currentRound, 1);
      expect(timer.currentDuration.inSeconds, roundDuration.inSeconds);
      expect(timer.currentState, RoundState.round);

      async.elapse(Duration(seconds: roundDuration.inSeconds - 10));
      expect(timer.currentRound, 1);
      expect(timer.currentDuration.inSeconds, 10);
      expect(timer.currentState, RoundState.round);

      timer.pauseTimer();

      async.elapse(Duration(seconds: roundDuration.inSeconds - 10));
      expect(timer.currentRound, 1);
      expect(timer.currentDuration.inSeconds, 10);
      expect(timer.currentState, RoundState.round);

      timer.startTimer();

      async.elapse(const Duration(seconds: 11));
      expect(timer.currentRound, 1);
      expect(timer.currentDuration.inSeconds, restDuration.inSeconds);
      expect(timer.currentState, RoundState.rest);

      async.elapse(Duration(seconds: restDuration.inSeconds - 8));
      expect(timer.currentRound, 1);
      expect(timer.currentDuration.inSeconds, 8);
      expect(timer.currentState, RoundState.rest);

      timer.pauseTimer();

      async.elapse(Duration(seconds: restDuration.inSeconds - 8));
      expect(timer.currentRound, 1);
      expect(timer.currentDuration.inSeconds, 8);
      expect(timer.currentState, RoundState.rest);

      timer.startTimer();

      async.elapse(const Duration(seconds: 9));
      expect(timer.currentRound, 2);
      expect(timer.currentDuration.inSeconds, roundDuration.inSeconds);
      expect(timer.currentState, RoundState.round);
    });
  });

  test('timer isTimerRunning', () async {
    fakeAsync((FakeAsync async) {
      const rounds = 1;
      const prepareDuration = Duration(seconds: 9);
      const roundDuration = Duration(seconds: 60);
      const restDuration = Duration(seconds: 15);
      TimerController timer =
      TimerController(rounds, prepareDuration, roundDuration, restDuration);

      expect(timer.isTimerRunning(), false);
      timer.startTimer();
      expect(timer.isTimerRunning(), true);

      async.elapse(Duration(seconds: prepareDuration.inSeconds - 4));
      expect(timer.currentRound, 1);
      expect(timer.currentDuration.inSeconds, 4);
      expect(timer.currentState, RoundState.prepare);
      expect(timer.isTimerRunning(), true);

      timer.pauseTimer();
      expect(timer.isTimerRunning(), false);
      timer.startTimer();
      expect(timer.isTimerRunning(), true);

      async.elapse(const Duration(seconds: 5));
      expect(timer.isTimerRunning(), true);
      timer.pauseTimer();
      expect(timer.isTimerRunning(), false);
    });
  });
}
